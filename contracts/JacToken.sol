// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract JacToken {

  uint256 public totalSupply;
  string public name = 'JacToken';
  string public symbol = 'JAC';
  string public version = 'JacToken v1.0';

  event Transfer(
    address indexed _from,
    address indexed _to,
    uint256 _value
  );

  event Approval(
    address indexed _owner,
    address indexed _spender,
    uint256 _value
  );

  mapping(address => uint256) public balanceOf;
  mapping(address => mapping(address => uint256)) public allowance;

  constructor(uint256 _initalSupply) {
    balanceOf[msg.sender] = _initalSupply; //msg.sender is account address of code that called this function.
    totalSupply = _initalSupply;
  }

  function transfer(address _to, uint256 _value) public returns (bool success){
    // callers public key address has been signed by wallet private key therefore can call on behalf of 
    // public key msg.sender.
    require(balanceOf[msg.sender] >= _value);

    balanceOf[msg.sender] -= _value;
    balanceOf[_to] += _value;

    emit Transfer(msg.sender, _to, _value);
    
    return true;
  }

  function approve(address _spender, uint256 _value) public returns (bool success) {
    allowance[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }

  // Performs transfer by 3rd party for approved accounts / allowances.
  /* 
    _from : address of account we're transfering from. The account the 3rd party is acting on behalf of. IE an exchange taking for a customer.
    _to : address of account we're transfering to which. IE a seller the exchange is buying from.
    msg.sender: caller account of function. The 3rd party. IE the exchange.
    _value : num tokens
  */
  function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
    require(balanceOf[_from] >= _value);
    require(allowance[_from][msg.sender] >= _value);
    allowance[_from][msg.sender] -= _value;
    balanceOf[_from] -= _value;
    balanceOf[_to] += _value;
    emit Transfer(_from, _to, _value);
    return true;
  }
 }