var JacToken = artifacts.require('../contracts/JacToken.sol');

contract('JacToken', function(accounts) {

    it('checks full balance is allocated to admin account on deployment', async function() {
        const tokenInstance = await JacToken.deployed();
        const adminBalance = await tokenInstance.balanceOf(accounts[0]);
        assert.equal(adminBalance.toNumber(), 10000000, 'admin has all 1M allocated on deployment.');
    });

})